package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	RESULT  []struct {
		ID     int     `json:"id"`
		Title  string  `json:"title"`
		Start  string  `json:"start"`
		End    string  `json:"end"`
		Budget float32 `json:"budget"`
		Status string  `json:"status"`
		ImgUrl string  `json:"imgUrl"`
		Place  Place   `json:"place"`
	} `json:"result"`
}

type Place struct {
	Longtitude float32 `json:"longtitude"`
	Latitude   float32 `json:"latitude"`
	PlaceName  string  `json:"placeName"`
}

func main() {
	handler := http.HandlerFunc(handleRequest)
	http.Handle("/example", handler)
	http.ListenAndServe(":3000", nil)
}

func handleRequest(w http.ResponseWriter, t *http.Request) {
	var resJson string = `{
		"status":"From dockerhub Test 05"
		"message":"ok",
		"result":[
			{
				"id":"1",
				"title":"Di choi",
				"start":"1618469582043",
				"end":"161846958091",
				"place":{
					"longtitude":35.5,
					"latitude":40.5,
					"placeName":"Da Nang"
				},
				"budget":100000.0,
				"status":"ONGOING",
				"imgUrl":"link"
			},
			{
				"id":"1",
				"title":"Di choi",
				"start":"1618469582043",
				"end":"161846958091",
				"place":{
					"longtitude":35.5,
					"latitude":40.5,
					"placeName":"Da Nang"
				},
				"budget":100000.0,
				"status":"ONGOING",
				"imgUrl":"link"
			},
			{
				"id":"1",
				"title":"Di choi",
				"start":"1618469582043",
				"end":"161846958091",
				"place":{
					"longtitude":35.5,
					"latitude":40.5,
					"placeName":"Da Nang"
				},
				"budget":100000.0,
				"status":"ONGOING",
				"imgUrl":"link"
			},
			{
				"id":"1",
				"title":"Di choi",
				"start":"1618469582043",
				"end":"161846958091",
				"place":{
					"longtitude":35.5,
					"latitude":40.5,
					"placeName":"Da Nang"
				},
				"budget":100000.0,
				"status":"ONGOING",
				"imgUrl":"link"
			}
		]
	}`

	var response Response
	json.Unmarshal([]byte(resJson), &response)

	w.Header().Set("Content-Type", "application/json")
	jsonResp, err := json.Marshal(response)

	if err != nil {
		log.Fatalf("Error happend in JSON marshal. Err: %s", err)
	}
	w.Write(jsonResp)
	return
}
